#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>

#define KNRM "\x1B[0m"
#define KRED "\x1B[31m"
#define KGRN "\x1B[32m"
#define KYEL "\x1B[33m"
#define KBLU "\x1B[34m"
#define KMAG "\x1B[35m"
#define KCYN "\x1B[36m"
#define KWHT "\x1B[37m"
#define RESET "\x1B[0m"

enum TokenType
{
    TOKEN_KEYWORD,
    TOKEN_IDENTIFIER,
    TOKEN_LITERAL,
    TOKEN_OPERATOR,
    TOKEN_DELIMITER,
    TOKEN_WHITESPACE
};

enum TokenSubType
{
    OP_MULT,      // '*'
    OP_LESSTHAN,  // '>'
    OP_ASSIGMENT, // '='

    DEL_LEFTPAR,        // '('
    DEL_RIGHTPAR,       // ')'
    DEL_COMMA,          // ','
    DEL_SEMICOLON,      // ';'
    DEL_DOUBLEPOINT,    // ':'
    DEL_LEFTCURLBRACE,  // '{'
    DEL_RIGHTCURLBRACE, // '}'

    KW_FLOAT,
    KW_IF,
    KW_ELSE,

    LIT_INTEGER, // 0
    LIT_FLOAT,   // 4.112 and 0.999
    LIT_STRING,  // "Result: \"%s\"\n%d"  and "not"

    NONE,
};

struct StringTable
{
    char *items;
    size_t capacity;
    size_t count;
    size_t rowCapacity;
};

struct TextPosition
{
    int x;
    int y;
};

struct Token
{
    char *lexeme;
    enum TokenType type;
    int subType;
    struct TextPosition position;
};

struct TokenList
{
    struct Token *items;
    size_t capacity;
    size_t count;
    char *SubtupeToPrint;
};

char *findString(const struct StringTable *ptable, const char *str);

bool containsString(const struct StringTable *ptable, const char *str);

void addString(struct StringTable *ptable, const char *str);

char *getStringAt(const struct StringTable *ptable, int index);

struct StringTable createStringTable(char *items, int cap, int rowCap);

struct TokenList createTokenList(struct Token *items, int cap);

void addToken(struct TokenList *plist, struct Token newToken);

void printList(struct TokenList *t_list);

struct Token *getToken(struct TokenList *t_list, int index);

bool readingNumber(char **p, char *pbuf);

enum TokenSubType checkOperatorsAndDelimeters(const char *word);

int main(int argc, char *argv[])
{
    char input_file[] = "input.txt";
    char output_file[] = "output.txt";

    bool print_table = false;

    for (int i = 1; i < argc; i++)
    {
        if ((strcmp(argv[i], "-o") != 0) && (strcmp(argv[i], "-l") != 0) && i == 1)
        {
            strcpy(input_file, (argv[i]));
            continue;
        }

        if (strcmp(argv[i], "-o") == 0)
        {
            i++;
            if (i == argc)
            {
                printf("ERROR (output file missing)\n");
                return 1;
            }
            strcpy(output_file, (argv[i])); //change the name of output file
            continue;
        }

        if (strcmp(argv[i], "-l") == 0)
        {
            print_table = true;
        }

    }

    const int nTokens = 100;
    struct Token aTokens[nTokens];
    struct TokenList tokens = createTokenList(&aTokens[0], nTokens);
    char input[100];

    FILE *fp = fopen(input_file, "r");

    if (fp == NULL)
    {
        //error
        return 1;
    }
    char symbol = fgetc(fp); // read the first symbol in input.txt
    int i = 0;
    while (symbol != EOF)
    {
        input[i] = symbol;
        i++;
        symbol = fgetc(fp);
    }
    input[i] = '\0';
    fclose(fp);

    //
    const int nRows = 50;
    const int nCols = 50;
    char mKeywords[nRows][nCols];
    struct StringTable keywords = createStringTable(&mKeywords[0][0], nRows, nCols);
    //
    const int nIdRowls = 50;
    const int nIdCols = 50;
    char mIdentifiers[nIdRowls][nIdCols];
    struct StringTable identifiers = createStringTable(&mIdentifiers[0][0], nIdRowls, nIdCols);
    //

    const int nLtRowls = 50;
    const int nLtCols = 50;
    char mLiterals[nLtRowls][nLtCols];
    struct StringTable literals = createStringTable(&mLiterals[0][0], nLtRowls, nLtCols);
    //
    const int spRows = 50;
    const int spCols = 50;
    char mSpaces[nRows][nCols];
    struct StringTable whitespaces = createStringTable(&mSpaces[0][0], spRows, spCols);
    //
    addString(&keywords, "float");
    addString(&keywords, "if");
    addString(&keywords, "else");
    //
    struct TextPosition pos;
    pos.x = 1;
    pos.y = 1;

    //
    char *p = input;
    char buffer[100];
    char *pbuf = buffer;
    while (true)
    {
        //==========================readString
        if (*p == '"')
        {

            struct Token token;
            token.position.x = pos.x;
            token.position.y = pos.y;

            p++;
            while (true)
            {
                if (*p == '\"' && *(p - 1) != '\\')
                {
                    p++;
                    break;
                }
                *pbuf = *p;
                pbuf++;
                p++;
            }
            *pbuf = '\0';
            pbuf = buffer;
            printf(KRED "\"%s\"" RESET, buffer);

            pos.x += strlen(pbuf);

            addString(&literals, buffer);

            token.type = TOKEN_LITERAL;
            token.subType = LIT_STRING;

            token.lexeme = findString(&literals, buffer);

            addToken(&tokens, token);
        }
        else if (isalpha(*p) || *p == '_')
        {
            //==========================readWord
            while (isalnum(*p) || *p == '_')
            {
                *pbuf = *p;
                pbuf++;
                p++;
            }
            *pbuf = '\0';
            pbuf = buffer;
            //
            if (containsString(&keywords, buffer)) //KEYWORDS
            {
                struct Token token;
                token.position.x = pos.x;
                token.position.y = pos.y;
                token.lexeme = findString(&keywords, buffer);

                token.type = TOKEN_KEYWORD;

                if (strcmp(buffer, "float") == 0)
                {
                    token.subType = KW_FLOAT;
                }
                else if (strcmp(buffer, "if") == 0)
                {
                    token.subType = KW_IF;
                }
                else if (strcmp(buffer, "else") == 0)
                {
                    token.subType = KW_ELSE;
                }
                printf(KGRN "%s" RESET, buffer);

                pos.x += strlen(buffer);

                addToken(&tokens, token);
            }
            else //IDENTIFIERS
            {
                addString(&identifiers, buffer);
                //
                struct Token token;
                token.position.x = pos.x;
                token.position.y = pos.y;
                token.lexeme = findString(&identifiers, buffer);
                token.type = TOKEN_IDENTIFIER;
                printf(KBLU "%s" RESET, buffer);

                pos.x += strlen(buffer);

                addToken(&tokens, token);
            }
        }
        else if (*p == '*' || *p == '=' || *p == '<' || *p == ',' || *p == ')' || *p == '(' || *p == '{' || *p == '}' || *p == ';')
        {
            struct Token token;
            token.position.x = pos.x;
            token.position.y = pos.y;

            if (*p == '*' || *p == '=' || *p == '<')
            {
                *pbuf = *p;
                pbuf++;
                p++;
                *pbuf = '\0';
                pbuf = buffer;
                addString(&literals, buffer);

                printf(KYEL "%s" RESET, buffer);

                token.type = TOKEN_OPERATOR;
                token.subType = checkOperatorsAndDelimeters(buffer);
                token.lexeme = findString(&literals, buffer);
            }
            else
            {
                *pbuf = *p;
                pbuf++;
                p++;
                *pbuf = '\0';
                pbuf = buffer;
                addString(&literals, buffer);

                printf(KMAG "%s" RESET, buffer);

                token.type = TOKEN_DELIMITER;
                token.subType = checkOperatorsAndDelimeters(buffer);
                token.lexeme = findString(&literals, buffer);
            }

            pos.x += strlen(buffer);
            addToken(&tokens, token);
        }
        else if (isdigit(*p))
        {
            bool isFloat = readingNumber(&p, pbuf);
            pbuf = buffer;

            struct Token token;
            token.position.x = pos.x;
            token.position.y = pos.y;
            addString(&literals, buffer);

            token.type = TOKEN_LITERAL;

            if (isFloat)
            {
                token.subType = LIT_FLOAT;
            }
            else
            {
                token.subType = LIT_INTEGER;
            }

            printf(KCYN "%s" RESET, buffer);

            token.lexeme = findString(&literals, buffer);
            pos.x += strlen(buffer);
            addToken(&tokens, token);
        }
        else
        {
            addString(&whitespaces, p);
            p++;
            pos.x++;
            printf(" ");
        }
        if (*p == '\0')
        {
            puts(" ");
            break;
        }

        if (*p == '\n')
        {
            pos.y++;
            pos.x = 1;
            puts("");
        }
    }

    printf("\n\n\n");

    if(print_table)
    {
        printList(&tokens);
    }
    
    FILE *text = freopen(output_file, "w", stdout);

    fprintf(text, "%s", input);
    printf("\n\n");

    if(print_table)
    {
        printList(&tokens);
    }
    fclose(text);

    return 0;
}

void addString(struct StringTable *ptable, const char *str)
{
    int rowIndex = (*ptable).count;
    char *p = getStringAt(ptable, rowIndex);
    strcpy(p, str);
    (*ptable).count += 1;
}

char *getStringAt(const struct StringTable *ptable, int index)
{
    char *p = (*ptable).items;
    p += index * (*ptable).rowCapacity; //what is happening here? o_o
    return p;
}

struct StringTable createStringTable(char *items, int cap, int rowCap)
{
    struct StringTable table;
    table.items = items;
    table.capacity = cap;
    table.count = 0;
    table.rowCapacity = rowCap;
    return table;
}

char *findString(const struct StringTable *ptable, const char *str)
{
    for (int i = 0; i < (*ptable).count; i++)
    {
        char *p = getStringAt(ptable, i);
        if (strcmp(p, str) == 0)
        {
            return p;
        }
    }
    return NULL;
}

bool containsString(const struct StringTable *ptable, const char *str)
{
    return findString(ptable, str) != NULL;
}

void addToken(struct TokenList *plist, struct Token newToken)
{
    int prevCount = (*plist).count;
    int index = prevCount;
    (*plist).items[index] = newToken;
    int newCount = prevCount + 1;
    (*plist).count = newCount;
}

struct TokenList createTokenList(struct Token *items, int cap)
{
    struct TokenList list;
    list.items = items;
    list.capacity = cap;
    list.count = 0;
    return list;
}

enum TokenSubType checkOperatorsAndDelimeters(const char *word)
{

    if (strcmp(word, "*") == 0)
    {
        return OP_MULT;
    }
    else if (strcmp(word, "<") == 0)
    {
        return OP_LESSTHAN;
    }
    else if (strcmp(word, "(") == 0)
    {
        return DEL_LEFTPAR;
    }
    else if (strcmp(word, ")") == 0)
    {
        return DEL_RIGHTPAR;
    }
    else if (strcmp(word, ",") == 0)
    {
        return DEL_COMMA;
    }
    else if (strcmp(word, ";") == 0)
    {
        return DEL_SEMICOLON;
    }
    else if (strcmp(word, ":") == 0)
    {
        return DEL_DOUBLEPOINT;
    }
    else if (strcmp(word, "}") == 0)
    {
        return DEL_RIGHTCURLBRACE;
    }
    else if (strcmp(word, "{") == 0)
    {
        return DEL_LEFTCURLBRACE;
    }
    else if (strcmp(word, "=") == 0)
    {
        return OP_ASSIGMENT;
    }

    return NONE;
}

void printList(struct TokenList *t_list)
{
    for (int i = 0; i < t_list->count; i++)
    {
        struct Token tok;
        tok = *getToken(t_list, i);
        if (tok.type == TOKEN_KEYWORD)
        {
            printf("%-20s", "TOKEN_KEYWORD");
            if (tok.subType == KW_FLOAT)
            {
                printf("%-20s", "KW_FLOAT");
            }
            else if (tok.subType == KW_IF)
            {
                printf("%-20s", "KW_IF");
            }
            else if (tok.subType == KW_ELSE)
            {
                printf("%-20s", "KW_ELSE");
            }
        }
        else if (tok.type == TOKEN_IDENTIFIER)
        {
            printf("%-20s", "TOKEN_IDENTIFIER");
            printf("%-20s", ""); //-----------------
        }
        else if (tok.type == TOKEN_OPERATOR)
        {
            printf("%-20s", "TOKEN_OPERATOR"); //------------------------

            if (tok.subType == OP_LESSTHAN)
            {
                printf("%-20s", "OP_LESSTHAN");
            }
            else if (tok.subType == OP_MULT)
            {
                printf("%-20s", "OP_MULT");
            }
            else if (tok.subType == OP_ASSIGMENT)
            {
                printf("%-20s", "OP_ASSIGMENT");
            }
        }
        else if (tok.type == TOKEN_LITERAL)
        {
            printf("%-20s", "TOKEN_LITERAL"); //------------------
            if (tok.subType == LIT_FLOAT)
            {
                printf("%-20s", "LIT_FLOAT");
            }
            else if (tok.subType == LIT_INTEGER)
            {
                printf("%-20s", "LIT_INTEGER");
            }
            else if (tok.subType == LIT_STRING)
            {
                printf("%-20s", "LIT_STRING");
            }
        }
        else if (tok.type == TOKEN_DELIMITER)
        {
            printf("%-20s", "TOKEN_DELIMITER"); //----------------------
            if (tok.subType == DEL_LEFTCURLBRACE)
            {
                printf("%-20s", "DEL_LEFTCURLBRACE");
            }
            else if (tok.subType == DEL_RIGHTCURLBRACE)
            {
                printf("%-20s", "DEL_RIGHTCURLBRACE");
            }
            else if (tok.subType == DEL_COMMA)
            {
                printf("%-20s", "DEL_COMMA");
            }
            else if (tok.subType == DEL_LEFTPAR)
            {
                printf("%-20s", "DEL_LEFTPAR");
            }
            else if (tok.subType == DEL_RIGHTPAR)
            {
                printf("%-20s", "DEL_RIGHTPAR");
            }
            else if (tok.subType == DEL_SEMICOLON)
            {
                printf("%-20s", "DEL_SEMICOLON");
            }
        }
        printf("%d\t\t", tok.position.x);
        printf("%d\t\t", tok.position.y);
        printf("\"%s\"\n", tok.lexeme);
    }
}

struct Token *getToken(struct TokenList *t_list, int index)
{
    struct Token *tok = t_list->items;
    tok += index;
    return tok;
}

bool readingNumber(char **p, char *pbuf) //двойной указатель для того, чтобы двигать указатель на строку и переходить от одного элемента, к другому
{
    bool isFloat = false;
    while (isdigit(**p))
    {
        *pbuf = **p;
        pbuf++;
        (*p)++;
    }
    if (**p == '.')
    {
        *pbuf = **p;
        pbuf++;
        (*p)++;
        while (isdigit(**p))
        {
            *pbuf = **p;
            pbuf++;
            (*p)++;
        }
        isFloat = true;
    }
    *pbuf = '\0';
    return isFloat;
}