#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <progbase.h>

int main()
{
    float x = 0.0;
    float y = 0.0;
    float z = 0.0;
    float a0 = 0.0;
    float a1 = 0.0;
    float a2 = 0.0;
    float a = 0.0;
    printf("RAD: \n");
    printf("Enter x: ");
    scanf("%f", &x);
    printf("Enter y: ");
    scanf("%f", &y);
    printf("Enter z: ");
    scanf("%f", &z);
    if (x == y || x == 0.0 || z == 0.0 || (sin(x) < 0.01 && sin(x) > -0.01))
    // sin(Pi*n) = 0, but sin(x) is in denominator)
    {
        printf("ERROR\n");
    }
    else
    {
        a0 = (pow(x, (y + 1)) / (pow((x - y), (1 / z))));
        a1 = (5 * y) + (z / x);
        a2 = sqrt(fabs((cos(y) / sin(x)) + 2));
        a = a0 + a1 + a2;
        printf("x = %f\n", x);
        printf("y = %f\n", y);
        printf("z = %f\n", z);
        printf("a0 = %f\n", a0);
        printf("a1 = %f\n", a1);
        printf("a2 = %f\n", a2);
        printf("a = %f\n", a);
        return 0;
    }
}
