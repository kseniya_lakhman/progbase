// Компілювати за допомогою:
// gcc main.c -lprogbase -lm

#include <stdio.h>    // Для друку в термінал
#include <math.h>     // Для математичних функцій
#include <stdlib.h>   // Деякі додаткові функції
#include <progbase.h> // Спрощений ввід і вивід даних у консоль

int main()
{
    const float xmin = -10.0;
    const float xmax = 10.0;
    const float xstep = 0.5;

    const float gran1 = -4.5;
    const float gran2 = -1;
    const float gran3 = 1;
    const float gran4 = 4.5;

    float x = xmin;
    float y = 0.0;

    while (x <= xmax && x >= xmin)
    {
        if ((x >= gran1 && x < gran2) || ((x > gran3) && (x <= gran4)))
        {
            y = 1.5 * cos(0.2 * x) / (x + 1);
            printf("y(%.1f) = %f;\n", x, y);
            // x += xstep;
            // continue;
        }
        else
        {
            if (x == -1)
            {
                puts("ERROR");
                //   x += xstep;
                //  continue;
            }
            else
            {
                y = 2 / (3 * x + 3);
                printf("y(%.1f) = %f;\n", x, y);
                //  x += xstep;
                // continue;
            }
        }
        x += xstep;
    }

    return 0;
}
