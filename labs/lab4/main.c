#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <progbase.h>
#include <progbase/console.h>
#include <time.h>
#include <stdbool.h>

float float_rand(float rmin, float rmax);
int getColor(char colorCode);

int main()
{
    srand(time(0));

    Console_clear();

    //=====================
    char KEY1Input = 0;
    float suma = 0;
    int length = 0;

    int indexMax = 0;
    int indexMin = 0;

    float rmin = 0;
    float rmax = 0;

    int index = 0;

    //====================
    char KEY2Input = 0;
    char KEY3Input = 0;

    int indexMaxI = 0;
    int indexMaxJ = 0;
    int indexMinI = 0;
    int indexMinJ = 0;

    int line = 0;
    int column = 0;
    int number = 0;
    int suma_i = 0;

    int heigth = 0; //кількість рядків

    int min = 0;
    int max = 0;

    // ================
    bool mainMenuExisting = true;
    while (mainMenuExisting)
    {

        Console_clear();
        puts("a. Submenu 1");
        puts("s. Submenu 2");
        puts("d. Submenu 3");
        puts("q. Exit");

        char mainMenuUserInput = Console_getChar();

        switch (mainMenuUserInput)
        {
        case 'q':
        {
            mainMenuExisting = false;
            break;
        }
        case 'a':
        {
            printf("Enter the lenght of your array: N = ");
            scanf("%d", &length);
            float arr1[length];
            for (int k = 0; k < length; k++)
            {
                arr1[k] = 0.0;
            }
            bool isSubmenuARunning = true;
            while (isSubmenuARunning)
            {
                Console_clear();
                puts("Array: ");
                for (int i = 0; i < length; i++)
                {
                    printf("%10.3f |", arr1[i]);
                }
                puts(" ");
                puts("1. Заповнити масив випадковими числами від L до H.");
                puts("2. Обнулити всі елементи масиву.");
                puts("3. Знайти максимальний елемент масиву та його індекс.");
                puts("4. Вивести суму додатніх елементів масиву.");
                puts("5. Поміняти місцями значення максимального і мінімального елементів масиву.");
                puts("6. Помножити всі елементи масиву на введене число.");
                puts("0. Exit");
                puts(" ");
                if (KEY1Input == '3')
                {
                    printf("Maximum element: index %d and value %f.\n", index, rmax);
                }
                else if (KEY1Input == '4')
                {
                    printf("\nSum is: %.2f\n", suma);
                    suma = 0;
                }

                KEY1Input = Console_getChar();

                switch (KEY1Input)
                {
                case '0':
                {
                    isSubmenuARunning = false;
                    break;
                }
                case '1':
                {
                    printf("Input lowest number for massive: ");
                    scanf("%f", &rmin);
                    printf("Input highest number for massive: ");
                    scanf("%f", &rmax);

                    for (int i = 0; i < length; i++)
                    {
                        arr1[i] = float_rand(rmin, rmax);
                    }
                    break;
                }
                case '2':
                {
                    printf("zero your array:\n");
                    for (int i = 0; i < length; i++)
                    {
                        arr1[i] = 0;
                    }
                    break;
                }
                case '3':
                {
                    rmax = arr1[0];
                    for (int i = 1; i < length; i++)
                    {
                        if (arr1[i] > rmax)
                        {
                            rmax = arr1[i];
                            index = i;
                        }
                    }
                    break;
                }
                case '4':
                {
                    for (int i = 0; i < length; i++)
                    {
                        if (arr1[i] > 0)
                        {
                            suma += arr1[i];
                        }
                    }
                    break;
                }
                case '5':
                {
                    rmax = arr1[0];
                    for (int i = 1; i < length; i++)
                    {
                        if (arr1[i] > rmax)
                        {
                            rmax = arr1[i];
                            indexMax = i;
                        }
                    }
                    rmin = arr1[0];
                    for (int i = 1; i < length; i++)
                    {
                        if (arr1[i] < rmin)
                        {
                            rmin = arr1[i];
                            indexMin = i;
                        }
                    }
                    float temp = rmax;
                    arr1[indexMax] = rmin;
                    arr1[indexMin] = temp;
                    break;
                }
                case '6':
                {
                    float nuMeric = 0;
                    printf("Enter the numeric: ");
                    scanf("%f", &nuMeric);
                    for (int i = 0; i < length; i++)
                    {
                        arr1[i] = nuMeric * arr1[i];
                    }
                    break;
                }
                default:
                {
                    puts("print the correct number of length: ");
                    break;
                }
                }
            }
            break;
        }

        case 's':
        {
            printf("enter the heigth of your array: N = \n");
            scanf("%d", &heigth); //кількість рядків
            printf("Enter the length of your array: M = \n");
            scanf("%d", &length);

            int arr2[heigth][length];

            if (length > 0 && heigth > 0)
            {
                for (int i = 0; i < heigth; i++)
                {
                    for (int j = 0; j < length; j++)
                    {
                        arr2[i][j] = 0.0;
                    }
                }

                //
                bool isSubmenuSRunning = true;
                while (isSubmenuSRunning)
                {

                    Console_clear();
                    puts("Matrix: ");
                    for (int i = 0; i < heigth; i++)
                    {
                        for (int j = 0; j < length; j++)
                        {
                            printf("|%5d | ", arr2[i][j]);
                        }
                        puts("");
                    }

                    puts(" ");
                    puts("1. Заповнити масив випадковими числами від L до H.");
                    puts("2. Обнулити всі елементи масиву.");
                    puts("3. Знайти мінімальний елемент та його індекси (i та j)");
                    puts("4. Знайти суму елементів рядка за заданим індексом.");
                    puts("5. Поміняти місцями значення максимального і мінімального елементів масиву");
                    puts("6. Змінити значення елементу за вказаними індексами на задане.");
                    puts("0. Exit");
                    puts(" ");

                    printf("\n");
                    if (KEY2Input == '3')
                    {
                        printf("Minimal element is: %d. His coordinates [%d][%d]\n", min, indexMinI, indexMinJ);
                    }
                    else if (KEY2Input == '4')
                    {
                        if (line <= length)
                        {
                            printf("suma (line %d) = %d\n", line, suma_i);
                            suma_i = 0;
                        }
                        else
                        {
                            printf("your numbes is uncorrect\n");
                        }
                    }

                    KEY2Input = Console_getChar();

                    switch (KEY2Input)
                    {

                    case '0':
                    {
                        isSubmenuSRunning = false;
                        break;
                    }
                    case '1':
                    {
                        for (int i = 0;; i++)
                        {
                            printf("Input the lowest number for massive: ");
                            scanf("%d", &min);
                            printf("min =: %d\n", min);

                            printf("Input the highest number for massive: ");
                            scanf("%d", &max);
                            printf("max =: %d\n", max);

                            if (max >= min)
                            {
                                for (int i = 0; i < heigth; i++)
                                {
                                    for (int j = 0; j < length; j++)
                                    {
                                        arr2[i][j] = rand() % (max - min + 1) + min;
                                    }
                                    puts("");
                                }
                                printf("\n");
                                break;
                            }
                            else
                            {
                                printf("\nYour numbers are uncorrect. Please,reenter them\n\n");
                            }
                        }
                        break;
                    }
                    case '2':
                    {
                        for (int i = 0; i < heigth; i++)
                        {
                            for (int j = 0; j < length; j++)
                            {
                                arr2[i][j] = 0;
                            }
                            puts("");
                        }
                        break;
                    }
                    case '3':
                    {
                        min = arr2[0][0];
                        for (int i = 0; i < heigth; i++)
                        {
                            for (int j = 0; j < length; j++)
                            {
                                if (arr2[i][j] < min)
                                {
                                    min = arr2[i][j];
                                    indexMinI = i;
                                    indexMinJ = j;
                                }
                            }
                        }
                        break;
                    }
                    case '4':
                    {
                        printf("enter the index of line: "); // lines
                        scanf("%d", &line);
                        if (line <= length)
                        {
                            for (int j = 0; j < length; j++)
                            {
                                suma_i += arr2[line][j];
                            }
                        }
                        else
                        {
                            break;
                        }
                        break;
                    }
                    case '5':
                    {
                        min = arr2[0][0];
                        for (int i = 0; i < heigth; i++)
                        {
                            for (int j = 0; j < length; j++)
                            {
                                if (arr2[i][j] <= min)
                                {
                                    min = arr2[i][j];
                                    indexMinI = i;
                                    indexMinJ = j;
                                }
                            }
                        }

                        max = arr2[0][0];
                        for (int i = 0; i < heigth; i++)
                        {
                            for (int j = 0; j < length; j++)
                            {
                                if (arr2[i][j] >= max)
                                {
                                    max = arr2[i][j];
                                    indexMaxI = i;
                                    indexMaxJ = j;
                                }
                            }
                        }

                        int temp = min;
                        arr2[indexMinI][indexMinJ] = max;
                        arr2[indexMaxI][indexMaxJ] = temp;

                        break;
                    }
                    case '6':
                    {
                        printf("Input index of line: ");
                        scanf("%d", &line);
                        printf("Input index of column: ");
                        scanf("%d", &column);
                        printf("Input your number: ");
                        scanf("%d", &number);

                        arr2[line][column] = number;
                    }

                    default:
                    {
                        break;
                    }
                    }
                }
                break;
            }
            break;
        }

        case 'd':
        {
            char KEY3INPUT = 0;
            do
            {
                const char image[28][28] = {
                    {0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1},
                    {0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1},
                    {0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1},
                    {0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1},
                    {0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1},
                    {0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1},
                    {0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0x0, 0x0, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0x0, 0x0, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF},
                    {0xF, 0xF, 0xF, 0xF, 0x0, 0x0, 0xF, 0xF, 0x0, 0x0, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0x0, 0x0, 0xF, 0xF, 0x0, 0x0, 0xF, 0xF, 0xF, 0xF},
                    {0xF, 0xF, 0xF, 0xF, 0x0, 0x0, 0xF, 0xF, 0xE, 0xE, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0xE, 0xE, 0xF, 0xF, 0x0, 0x0, 0xF, 0xF, 0xF, 0xF},
                    {0xF, 0xF, 0x0, 0x0, 0xE, 0xE, 0xF, 0xF, 0xF, 0xF, 0xE, 0xE, 0xE, 0xE, 0xF, 0xF, 0xE, 0xE, 0xF, 0xF, 0xF, 0xF, 0xE, 0xE, 0x0, 0x0, 0xF, 0xF},
                    {0xF, 0xF, 0x0, 0x0, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0x0, 0x0, 0xF, 0xF},
                    {0x0, 0x0, 0xE, 0xE, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xE, 0xE, 0x0, 0x0},
                    {0x0, 0x0, 0xF, 0xF, 0xF, 0xF, 0x0, 0x0, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0x0, 0x0, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF},
                    {0x0, 0x0, 0xF, 0xF, 0x2, 0x2, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0x0, 0x0, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0x2, 0x2, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF},
                    {0x0, 0x0, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0x0, 0x0, 0xF, 0xF, 0x0, 0x0, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF},
                    {0x0, 0x0, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF},
                    {0x0, 0x0, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF},
                    {0x0, 0x0, 0xE, 0xE, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF},
                    {0x0, 0x0, 0xE, 0xE, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF},
                    {0x0, 0x0, 0xE, 0xE, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF},
                    {0xF, 0xF, 0x0, 0x0, 0xE, 0xE, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF},
                    {0xF, 0xF, 0xF, 0xF, 0x0, 0x0, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF, 0xF},
                    {0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1},
                    {0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1},
                    {0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1},
                    {0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1},
                    {0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1},
                    {0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1}};
                Console_clear();

                for (int i = 0; i < 28; i++)
                {
                    for (int j = 0; j < 28; j++)
                    {
                        int color = getColor(image[i][j]);
                        Console_setCursorAttribute(color);
                        printf("  ");
                        Console_reset();
                    }
                    printf("\n");
                }
                printf("Exit: press 'q'\n");

                KEY3INPUT = Console_getChar();
            } while (KEY3INPUT != 'q');
            break;
        }

        default:
        {
            break;
        }
        }
    }

    puts("Bye!");

    return 0;
}

float float_rand(float rmin, float rmax)
{
    float scale = rand() / (float)RAND_MAX; /* [0, 1.0] */
    return rmin + scale * (rmax - rmin);    /* [min, max] */
}

int getColor(char colorCode)
{
    // colors encoding table (hex code -> console color)
    const char colorsTable[16][2] = {
        {0x0, BG_BLACK},
        {0x1, BG_INTENSITY_BLACK},
        {0x2, BG_RED},
        {0x3, BG_INTENSITY_RED},
        {0x4, BG_GREEN},
        {0x5, BG_INTENSITY_GREEN},
        {0x6, BG_YELLOW},
        {0x7, BG_INTENSITY_YELLOW},
        {0x8, BG_BLUE},
        {0x9, BG_INTENSITY_BLUE},
        {0xa, BG_MAGENTA},
        {0xb, BG_INTENSITY_MAGENTA},
        {0xc, BG_CYAN},
        {0xd, BG_INTENSITY_CYAN},
        {0xe, BG_WHITE},
        {0xf, BG_INTENSITY_WHITE}};
    const int tableLength = sizeof(colorsTable) / sizeof(colorsTable[0]);
    for (int i = 0; i < tableLength; i++)
    {
        char colorPairCode = colorsTable[i][0];
        char colorPairColor = colorsTable[i][1];
        if (colorCode == colorPairCode)
        {
            return colorPairColor; // we have found our color
        }
    }
    return 0; // it's an error
}