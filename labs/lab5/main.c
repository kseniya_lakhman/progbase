#include <stdio.h>    // Для друку в термінал
#include <math.h>     // Для математичних функцій
#include <stdlib.h>   // Деякі додаткові функції
#include <progbase.h> // Спрощений ввід і вивід даних у консоль
#include <progbase/console.h>
#include <time.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>

int conReadLine(char str[], int maxBufLen)
{
    fgets(str, maxBufLen, stdin);
    int bufLength = strlen(str);
    if (str[bufLength - 1] == '\n')
    {
        str[bufLength - 1] = '\0';
        bufLength -= 1;
    }
    else
    {
        for (char ch; (ch = getchar()) != '\n';)
        {
        }
    }
    return bufLength;
}

int main()
{
    //===============
    int min = 33;
    int max = 127;
    //===============

    //===============
    bool isMainMenuRunning = true;
    while (isMainMenuRunning)
    {
        //

        Console_clear();

        puts("1. Characters ");
        puts("2. String");
        puts("3. Quit");

        char KEY_MAIN = Console_getChar();
        switch (KEY_MAIN)
        {

        //=====================
        case '1':
        {

            bool isSubmenu1Running = true;
            while (isSubmenu1Running)
            {
                Console_clear();
                puts("");
                puts("1. Alphanumeric");
                puts("2. Alphabetic (lowercase)");
                puts("3. Alphabetic (uppercase)");
                puts("4. Alphabetic (all)");
                puts("5. Decimal digit");
                puts("6. Hexadecimal digit");
                puts("7. Punctuation");
                puts("0. < Back");

                printf("> Enter your option: ");

                char menu1Input = Console_getChar();
                switch (menu1Input)
                {

                case '0':
                {
                    isSubmenu1Running = false;
                    break;
                }
                case '1':
                {
                    for (int i = min; i <= max; i++)
                    {
                        char ch = i;
                        if (isalnum(ch))
                        {
                            putchar(ch);
                            printf(" ");
                        }
                    }
                    break;
                }
                case '2':
                {
                    for (int i = min; i <= max; i++)
                    {
                        char ch = i;
                        if (islower(ch))
                        {
                            putchar(ch);
                            printf(" ");
                        }
                    }
                    break;
                }
                case '3':
                {
                    for (int i = min; i <= max; i++)
                    {
                        char ch = i;
                        if (isupper(ch))
                        {
                            putchar(ch);
                            printf(" ");
                        }
                    }
                    break;
                }
                case '4':
                {
                    for (int i = min; i <= max; i++)
                    {
                        char ch = i;
                        if (isalpha(ch))
                        {
                            putchar(ch);
                            printf(" ");
                        }
                    }
                    break;
                }
                case '5':
                {
                    for (int i = min; i <= max; i++)
                    {
                        char ch = i;
                        if (isdigit(ch))
                        {
                            putchar(ch);
                            printf(" ");
                        }
                    }
                    break;
                }
                case '6':
                {
                    for (int i = min; i <= max; i++)
                    {
                        char ch = i;
                        if (isxdigit(ch))
                        {
                            putchar(ch);
                            printf(" ");
                        }
                    }
                    break;
                }

                case '7':
                {
                    for (int i = min; i <= max; i++)
                    {
                        char ch = i;
                        if (ispunct(ch))
                        {
                            putchar(ch);
                            printf(" ");
                        }
                    }
                    break;
                }
                default:
                {
                    break;
                }
                }
            }

            break;
        }
            //=====================
        case '2':
        {
            srand(time(0));
            Console_clear();
            int menu2Input = 0;
            int q = 1;
            int random = 0;
            int bufLen = 0;

            printf("Enter the length of buf (bufLen): ");
            scanf("%d", &bufLen);
            char buf[bufLen];

            for (char ch; (ch = getchar()) != '\n';)
            {
            }
            //random generation
            for (int i = 0; i <= bufLen; i++)
            {
                if (i < bufLen)
                {
                    random = rand() % ((126 - 33 + 1) + 33);
                    if (isgraph(random))
                    {
                        buf[i] = random;
                    }
                    else
                    {
                        i = i - 1;
                    }
                }
                else
                {
                    buf[i] = '\0';
                }
            }
            //========

            int k = 1;
            while (k)
            {
                Console_clear();

                puts("");
                puts("1. Замінити рядок у масиві на введене рядкове значення із консолі");
                puts("2. Очистити рядок");
                puts("3. Вивести підрядок із заданої позиції і заданої довжини.");
                puts("4. Вивести список (непустих) підрядків, розділених заданим символом.");
                puts("5. Вивести найдовше слово");
                puts("6. Знайти та вивести всі цілі числа, що містяться у рядку");
                puts("7. Знайти та вивести суму всіх дробових чисел, що містяться у рядку");
                puts("0. < Back");
                if (q > 0)
                {
                    switch (menu2Input)
                    {
                        //
                    case '1':
                    {
                        if (bufLen == 0 || buf[0] == '\0')
                        {
                            printf("Your length is 0. Enter the length: ");
                            scanf("%d", &bufLen);

                            for (char ch; (ch = getchar()) != '\n';)
                            {
                            }

                            printf("Enter string: ");
                            int strLen = conReadLine(buf, bufLen + 1);

                            printf("Entered (%d): \"%s\"", strLen, buf);
                            puts("\n");
                        }
                        else
                        {
                            printf("Enter string: ");
                            int strLen = conReadLine(buf, bufLen + 1);

                            printf("Your string (%d): \"%s\"", strLen, buf);
                            puts("\n");
                        }
                        break;
                    }
                    case '2':
                    {
                        buf[0] = '\0';
                        bufLen = strlen(buf);
                        printf("Your string (%d): \"%s\"", bufLen, buf);
                        puts("\n");
                        break;
                    }
                    case '3':
                    {
                        int position = 0;
                        int length = 0;

                        printf("Enter your position: ");
                        scanf("%d", &position);

                        printf("Enter your length: ");
                        scanf("%d", &length);

                        char tempString[length + 1];
                        tempString[length] = '\0';

                        strncpy(tempString, buf + position, length);

                        if (position + length <= strlen(buf) && position >= 0 && position <= strlen(buf) && length >= 0)
                        {
                            strncpy(tempString, buf + position, length);
                            printf("Your string: \"%s\"", tempString);
                        }
                        else
                        {
                            printf("Incorrect position or length");
                        }

                        for (char ch; (ch = getchar()) != '\n';)
                        {
                        }
                        break;
                    }
                    case '4':
                    {
                        char symbol = 0;
                        printf("enter the symbol: ");
                        scanf("%c", &symbol);
                        for (char ch; (ch = getchar()) != '\n';)
                        {
                        }

                        char str[bufLen];
                        int bufX = 0;

                        for (int i = 0;; i++)
                        {

                            if (buf[i] == symbol || buf[i] == '\0')
                            {
                                if (bufX != 0)
                                {
                                    str[bufX] = '\0';
                                    printf("'%s'\n", str);
                                    bufX = 0;
                                }

                                if (buf[i] == '\0')
                                {
                                    break;
                                }
                            }
                            else
                            {
                                str[bufX] = buf[i];
                                bufX += 1;
                            }
                        }
                        break;
                    }
                    case '5':
                    {
                        int index = 0;

                        char temptSTR[bufLen];
                        char maxSRT[bufLen];
                        int maxLen = 0;

                        char ch = 0;
                        int c = 0;

                        for (int i = 0; i < strlen(buf); i++)
                        {
                            ch = buf[i];
                            if (isalpha(ch) && ch != '\0')
                            {
                                temptSTR[c] = ch;
                                c++;
                            }
                            else
                            {
                                temptSTR[c] = '\0';
                                if (strlen(temptSTR) > maxLen)
                                {
                                    strcpy(maxSRT, temptSTR);
                                    maxLen = strlen(temptSTR);
                                }
                                c = 0;
                            }
                        }
                        printf("The biggest word: '%s'", maxSRT);
                        break;
                    }
                    case '6':
                    {
                        char str[bufLen];
                        char ch = 0;
                        int bufX = 0;

                        for (int i = 0;; i++)
                        {
                            ch = buf[i];
                            if (isdigit(ch))
                            {
                                str[bufX] = ch;
                                bufX += 1;
                            }
                            else
                            {
                                if (bufX != 0)
                                {
                                    str[bufX] = '\0';
                                    printf("Number: '%s'\n", str);
                                    bufX = 0;
                                }
                                if (ch == '\0')
                                {
                                    break;
                                }
                            }
                        }
                        break;
                    }
                    case '7':
                    {
                        double sum = 0;
                        int c = -1;
                        bool existDot = false;
                        for (int i = 0; i < bufLen; i++)
                        {
                            if (!isdigit(buf[i]) && buf[i] != '.' && c != -1 && (!existDot || buf[i - 1] == '.'))
                            { //якщо buf не число, не точка, і ми ще не почали зчитувати число, точка існує або число виду .4
                                c = -1;
                                existDot = false;
                            }

                            if (c == -1 && isdigit(buf[i]))
                                c = i;

                            if (c != -1 && existDot && !isdigit(buf[i]) && buf[i - 1] != '.')
                            {
                                bool foundDot = false;
                                double cur = 0;
                                int countDigitsAfterDot = 0;
                                for (int j = c; j < i; j++)
                                {
                                    if (isdigit(buf[j]))
                                    {
                                        cur *= 10;
                                        cur += buf[j] - '0';
                                        if (foundDot)
                                            countDigitsAfterDot++;
                                    }
                                    else
                                        foundDot = true;
                                }
                                sum += cur / pow(10, countDigitsAfterDot);
                                c = -1;
                                existDot = false;
                            }

                            if (c != -1 && !existDot && buf[i] == '.')
                                existDot = true;
                        }
                        if (c != -1 && existDot && buf[bufLen - 1] != '.')
                        {
                            bool foundDot = false;
                            double cur = 0;
                            int countDigitsAfterDot = 0;
                            for (int j = c; j < bufLen; j++)
                            {
                                if (isdigit(buf[j]))
                                {
                                    cur *= 10;
                                    cur += buf[j] - '0';
                                    if (foundDot)
                                        countDigitsAfterDot++;
                                }
                                else
                                    foundDot = true;
                            }
                            sum += cur / pow(10, countDigitsAfterDot);
                        }
                        printf("The following sum is %0.3lf\n", sum);
                        break;
                    }
                    case '0':
                    {
                        k = 0;
                        q = 0;
                        break;
                    }
                    default:
                    {
                        break;
                    }
                        //
                    }
                    //
                }

                printf("\nString you are working with: ");
                puts(buf);
                menu2Input = Console_getChar();
            }
            break;
        }
        case '3':
        {
            isMainMenuRunning = false;
            break;
        }
        }
        //
    }
    return 0;
    
}