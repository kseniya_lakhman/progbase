#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <progbase.h>
#include <progbase/canvas.h>
#include <progbase/console.h>
#include <time.h>
#include <string.h>
#include <assert.h>

struct vec_2d
{
    float x;
    float y;
};

struct vec_2d create(float x, float y)
{
    struct vec_2d v = {x, y};
    return v;
};

struct color
{
    int red;
    int green;
    int blue;
};

struct Ball
{
    struct vec_2d vel;
    struct vec_2d loc;
    float y0;
    struct color col;
};

struct Ball updateBall(struct Ball b, int width, int height, int i, int left_side, int right_side);
struct Ball createBall(int width, int height, int i, int N, int left_side, int right_side);

void drawBall(struct Ball b, int width, int height, int radius_ball);
void drawLines(int i, struct Ball l[i], int nballs);
void draw_main_vertical_Line(int width, int height);

float float_random(int x, int y);
void mainTest();

float length(struct vec_2d v);
struct Ball negative(struct Ball neg_ball);
struct vec_2d add(struct vec_2d a, struct vec_2d b);
struct vec_2d mult(struct vec_2d v, float n);
struct vec_2d norm(struct vec_2d v);
float distance(struct vec_2d a, struct vec_2d b);
struct vec_2d rotate(struct vec_2d v, float angle);
float angle(struct vec_2d v);
struct vec_2d fromPolar(float angle, float length);
int equals(struct vec_2d a, struct vec_2d b);

int main(int argc, char *argv[argc])
{
   
    if (strcmp(argv[1], "-t") == 0)
    {
        mainTest();
        return 0;
    }
    else if (strcmp(argv[1], "-n") == 0)
    {

        Console_clear();
        srand(time(NULL));

        int delay = 33;

        struct ConsoleSize consoleSize = Console_size();

        int width = consoleSize.columns;
        int height = consoleSize.rows * 2;

        int radius = 6;
        int left_side = -40;
        int right_side = 40;

        Canvas_setSize(width, height - 2);
        Canvas_invertYOrientation();

        int nballs = atoi(argv[2]);

        struct Ball balls[nballs];

        for (int i = 0; i < nballs; i++)
        {
            balls[i] = createBall(width, height, i, nballs, left_side, right_side);
        }

        while (1)
        {
            for (int i = 0; i < nballs; i++)
            {
                balls[i] = updateBall(balls[i], width, height, i, left_side, right_side);
            }

            Canvas_beginDraw();

            for (int i = 0; i < nballs - 1; i++)
            {
                draw_main_vertical_Line(width, height);
                drawLines(i, balls, nballs);
            }

            for (int i = 0; i < nballs; i++)
            {
                drawBall(balls[i], width, height, radius);
                drawBall(balls[i], width, height, radius - 2);
                drawBall(balls[i], width, height, radius - 4);
            }

            Canvas_endDraw();
            sleepMillis(delay);
        }
        return 0;
    }
}

//==================FUNCTIONS========================

struct Ball createBall(int width, int height, int i, int N, int left_side, int right_side)
{
    struct Ball ball_new;
    ball_new.loc.x = width / 2 + float_random(left_side, right_side);
    ball_new.y0 = (height / (N + 1)) * (i + 1);

    ball_new.vel.y = 0;
    ball_new.vel.x = float_random(-5, 5);

    ball_new.col.red = float_random(1, 255);
    ball_new.col.green = float_random(1, 255);
    ball_new.col.blue = float_random(1, 125);

    return ball_new;
}
//==========================

struct Ball updateBall(struct Ball new_ball, int width, int height, int i, int left_side, int right_side) //UPDATE____Ball
{
    new_ball.loc.x += new_ball.vel.x;
    new_ball.loc.y = new_ball.y0 + sin(new_ball.loc.x / 6) * 7;

    if (new_ball.loc.x <= width / 2 + left_side || new_ball.loc.x >= width / 2 + right_side)
    {
        new_ball = negative(new_ball);
    }

    return new_ball;
}
//==========================

float float_random(int x, int y)
{
    float k = (float)rand() / (float)(RAND_MAX / (y - x)) + x;
    return k;
}
//==========================

void drawBall(struct Ball b, int width, int height, int radius_ball)
{
    Canvas_setColorRGB(b.col.red, b.col.green, b.col.blue);
    Canvas_strokeCircle(b.loc.x, b.loc.y, radius_ball);
}

//==========================
void draw_main_vertical_Line(int width, int height)
{
    Canvas_setColorRGB(235, 235, 235);
    Canvas_strokeLine(width / 2, 0, width / 2, height);
}
//==========================

void drawLines(int i, struct Ball l[i], int nballs)
{
    Canvas_strokeLine(l[i].loc.x, l[i].loc.y, l[i + 1].loc.x, l[i + 1].loc.y);
}
//==========================

float length(struct vec_2d v)
{
    float length = sqrt(v.x * v.x + v.y * v.y);
    return length;
}
//==========================

struct Ball negative(struct Ball neg_ball)
{
    neg_ball.vel.x = -neg_ball.vel.x;
    return neg_ball;
}
//==========================

struct vec_2d add(struct vec_2d a, struct vec_2d b)
{
    struct vec_2d sum;
    sum.x = a.x + b.x;
    sum.y = a.y + b.y;
    return sum;
}
//==========================
struct vec_2d mult(struct vec_2d v, float n)
{
    struct vec_2d multVec = v;
    multVec.x *= n;
    multVec.y *= n;
    return multVec;
}
//=========================
struct vec_2d norm(struct vec_2d v)
{
    float inv_length = 1 / length(v);
    v.x *= inv_length;
    v.y *= inv_length;
    return v;
}
//=========================
float distance(struct vec_2d a, struct vec_2d b)
{
    float ab_distance = sqrt(pow((b.x - a.x), 2) + pow((b.y - a.y), 2));
    return ab_distance;
}
//=========================
struct vec_2d rotate(struct vec_2d v, float angle)
{
    struct vec_2d rotate_vec;
    rotate_vec.x = v.x * cos(angle) - v.y * sin(angle);
    rotate_vec.y = v.y * cos(angle) + v.x * sin(angle);
    return rotate_vec;
}
//===========================
float angle(struct vec_2d v)
{
    return atan(v.y / v.x);
}
//==========================
struct vec_2d fromPolar(float angle, float length)
{
    struct vec_2d polar_vec;
    polar_vec.x = length * cos(angle);
    polar_vec.y = length * sin(angle);
    return polar_vec;
}
//==========================
int equals(struct vec_2d a, struct vec_2d b)
{
    int result = 1;
    if (a.x == b.x && a.y == b.y)
    {
        result = 0;
    }
    return result;
}

void mainTest()
{
    struct vec_2d test_1 = create(1, 0);
    struct vec_2d test_2 = create(1, 1);
    struct vec_2d test_3 = create(-1, -1);
    struct vec_2d test_4 = create(-1, 1);
    struct vec_2d test_5 = create(-1, 0);
    struct vec_2d test_6 = create(1, 1);
    struct vec_2d test_7 = create(2, -1);
    struct vec_2d test_8 = create(1, 0);

    assert(distance(test_5, test_8) == 2);

    assert(length(test_2) < 1.5);
    assert(length(test_1) == 1);

    assert((angle(test_1)) == 0);
    assert((angle(test_2) - 3.14 / 4) < 0.1);

    assert(float_random(5, 50) <= 50 || float_random(5, 50) >= 5);

    test_2 = add(test_1, test_2);
    assert(test_2.x == 2 && test_2.y == 1);

    test_2 = add(test_4, test_6);
    assert(test_2.x == 0 && test_2.y == 2);

    test_2 = mult(test_4, 2);
    assert(test_2.x == -2 && test_2.y == 2);

    test_2 = mult(test_4, 0);
    assert(test_2.x == 0 && test_2.y == 0);

    test_2 = norm(test_7);
    assert(test_2.x < 0.9 && fabs(test_2.y) < 0.51);

    test_2 = rotate(test_1, 0);
    assert((int)test_2.x == 1 && (int)test_2.y == 0);

    test_2 = fromPolar(0, 5);
    assert(test_2.x == 5 && (test_2.y) == 0);

    assert((equals(test_4, test_5)) == 1);

    struct Ball vector1;
    vector1.vel.x = -1;
    struct Ball vector2;
    vector2.vel.x = 1;
    struct Ball vector3 = negative(vector2);
    assert(vector1.vel.x == vector3.vel.x);

    printf("\nlab7 works correct \n");
}