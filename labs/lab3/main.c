#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <progbase.h>
#include <progbase/console.h>

void drawLine(int x0, int y0, int x1, int y1, char ch);

int main()
{
    Console_clear();
    // borders
    int cx0 = 2;
    int cy0 = 2;
    int cwidth = 80;
    int cheight = 40;

    // line
    int bx0 = 20;
    int by0 = 20;
    float ba = 2.5;
    float a_step = 0.15;
    float b_step = 1;

    do
    {
        // calculating of line function arguments
        float k = tan(ba);
        float b = by0 - (k * bx0);

        // coordinates of cross
        int fx1 = (cy0 - b) / k;
        int fy1 = cy0;
        int fx2 = (cheight + cy0 - b) / k;
        int fy2 = cheight + cy0;
        int fx3 = cx0;
        int fy3 = b;
        int fx4 = cwidth + cx0;
        int fy4 = (cwidth + cx0) * k + b;
        int fx_1 = 0;
        int fy_1 = 0;
        int fx_2 = 0;
        int fy_2 = 0;
        // dot (rect center)
        int dx0 = 40;
        int dy0 = k * dx0 + b;
        // rectangle
        int dw = 10;
        int dh = 6;
        int dx = 0;
        int dy = 0;
//+++++++++++
        int l = sqrt((pow(bx0 - dx0,2) + pow(by0 - dy0,2)));
        float dx1 = cos(ba) * l;
        float dy1 = sin(ba) * l;
        dx0 = bx0 + dx1;
        dy0 = by0 + dy1;
//+++++++++++

        // check the points of contours 
        if (fx1 >= cx0 && fx1 <= (cx0 + cwidth) && fy1 >= cy0 && fy1 <= (cy0 + cheight))
        {
            if (fx2 >= cx0 && fx2 <= (cx0 + cwidth) && fy2 >= cy0 && fy2 <= (cy0 + cheight))
            {
                fx_1 = fx1;
                fy_1 = fy1;
                fx_2 = fx2;
                fy_2 = fy2;
            }
            else if (fx3 >= cx0 && fx3 <= (cx0 + cwidth) && fy3 >= cy0 && fy3 <= (cy0 + cheight))
            {
                fx_1 = fx1;
                fy_1 = fy1;
                fx_2 = fx3;
                fy_2 = fy3;
            }
            else
            {
                fx_1 = fx1;
                fy_1 = fy1;
                fx_2 = fx4;
                fy_2 = fy4;
            }
        }
        else if (fx2 >= cx0 && fx2 <= (cx0 + cwidth) && fy2 >= cy0 && fy2 <= (cy0 + cheight))
        {
            if (fx3 >= cx0 && fx3 <= (cx0 + cwidth) && fy3 >= cy0 && fy3 <= (cy0 + cheight))
            {
                fx_1 = fx2;
                fy_1 = fy2;
                fx_2 = fx3;
                fy_2 = fy3;
            }
            else
            {
                fx_1 = fx2;
                fy_1 = fy2;
                fx_2 = fx4;
                fy_2 = fy4;
            }
        }
        else
        {
            fx_1 = fx3;
            fy_1 = fy3;
            fx_2 = fx4;
            fy_2 = fy4;
        }
        // draw borders
        int cy = 0;
        int cx = 0;

        for (cx = cx0; cx <= (cx0 + cwidth); cx += 1)
        {
            cy = cy0;
            Console_setCursorPosition(cy, cx);
            printf("*");
            cy = cy0 + cheight;
            Console_setCursorPosition(cy, cx);
            printf("*");
        }
        for (cy = cy0; cy <= cy0 + cheight; cy += 1)
        {
            cx = cx0;
            Console_setCursorPosition(cy, cx);
            printf("*");
            cx = cx0 + cwidth;
            Console_setCursorPosition(cy, cx);
            printf("*");
        }
        // draw line
        drawLine(fx_1, fy_1, fx_2, fy_2, '*');

        // draw rectangle
        for (dx = dx0 - dw / 2; dx <= (dx0 + dw / 2); dx += 1)
        {
            Console_setCursorAttribute(BG_CYAN);
            dy = dy0 - dh / 2;
            Console_setCursorPosition(dy, dx);
            printf("*");
            dy = dy0 + dh / 2;
            Console_setCursorPosition(dy, dx);
            printf("*");
            Console_reset();
        }
        for (dy = dy0 - dh / 2; dy <= dy0 + dh / 2; dy += 1)
        {
            Console_setCursorAttribute(BG_CYAN);
            dx = dx0 - dw / 2;
            Console_setCursorPosition(dy, dx);
            printf("*");
            dx = dx0 + dw / 2;
            Console_setCursorPosition(dy, dx);
            printf("*");
            Console_reset();
        }
//=======================================================
        int key = Console_getChar();
        Console_clear();

        if (key != 27)
        {
            switch (key)
            {
            case 'w':
            {
                by0 -= b_step;
                break;
            }
            case 's':
            {
                by0 += b_step;
                break;
            }
            case 'a':
            {
                bx0 -= b_step;
                break;
            }
            case 'd':
            {
                bx0 += b_step;
                break;
            }
            case 'q':
            {
                ba -= a_step;
                break;
            }
            case 'e':
            {
                ba += a_step;
                break;
            }
            }
        }
        else
        {
            break;
        }
    } while (1);

    Console_reset();
    printf("\n");
    return 0;
}

void drawLine(int x0, int y0, int x1, int y1, char ch)
{
    int dx = abs(x1 - x0);
    int dy = abs(y1 - y0);
    int sx = x1 >= x0 ? 1 : -1;
    int sy = y1 >= y0 ? 1 : -1;

    if (x0 > 0 && y0 > 0)
    {
        Console_setCursorPosition(y0, x0);
        putchar(ch);
    }

    if (dy <= dx)
    {
        int d = (dy << 1) - dx;
        int d1 = dy << 1;
        int d2 = (dy - dx) << 1;
        for (int x = x0 + sx, y = y0, i = 1; i <= dx; i++, x += sx)
        {
            if (d > 0)
            {
                d += d2;
                y += sy;
            }
            else
            {
                d += d1;
            }
            // draw point
            if (y > 0 && x > 0)
            {
                Console_setCursorPosition(y, x);
                putchar(ch);
            }
        }
    }
    else
    {
        int d = (dx << 1) - dy;
        int d1 = dx << 1;
        int d2 = (dx - dy) << 1;
        for (int y = y0 + sy, x = x0, i = 1; i <= dy; i++, y += sy)
        {
            if (d > 0)
            {
                d += d2;
                x += sx;
            }
            else
            {
                d += d1;
            }
            // draw point
            if (x > 0 && y > 0)
            {
                Console_setCursorPosition(y, x);
                putchar(ch);
            }
        }
    }
}