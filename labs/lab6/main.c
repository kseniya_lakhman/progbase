#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <progbase.h>
#include <progbase/canvas.h>
#include <progbase/console.h>
#include <time.h>
#include <string.h>

struct vec_2d
{
    float x;
    float y;
};

struct color
{
    int red;
    int green;
    int blue;
};

struct balls
{
    struct vec_2d vel;
    struct vec_2d loc;
    float y0;
    struct color col;
    float radius;
};

float random_org(int x, int y);

int main()
{
    Console_clear();
    srand(time(NULL));

    int nballs = 75;

    int left_side = -40;
    int right_side = 40;

    int delay = 33;

    struct ConsoleSize consoleSize = Console_size();
    const int width = consoleSize.columns;
    const int height = consoleSize.rows * 2;

    struct balls balls[nballs];

    for (int i = 0; i < nballs; i++)
    {
        balls[i].loc.x = width / 2 + random_org(left_side, right_side);
        balls[i].y0 = (height / (nballs + 1)) * (i + 1);

        balls[i].vel.y = 0;
        balls[i].vel.x = random_org(-5, 5);

        balls[i].col.red = random_org(1, 255);
        balls[i].col.green = random_org(1, 255);
        balls[i].col.blue = random_org(1, 255);

        balls[i].radius = 4;
    }

    Canvas_setSize(width, height - 2);
    Canvas_invertYOrientation();

    while (1)
    {

        for (int i = 0; i < nballs; i++)
        {
            balls[i].loc.x += balls[i].vel.x;
            balls[i].loc.y = balls[i].y0 + sin(balls[i].loc.x / 6) * 7;
            if (balls[i].loc.x <= width / 2 + left_side || balls[i].loc.x >= width / 2 + right_side)
            {
                balls[i].vel.x = -balls[i].vel.x;
            }
        }

        Canvas_beginDraw();

        Canvas_setColorRGB(15, 206, 54);
        Canvas_strokeLine(width / 2, 0, width / 2, height);

        for (int i = 0; i < nballs; i++)
        {
            Canvas_setColorRGB(balls[i].col.red, balls[i].col.green, balls[i].col.blue);
            Canvas_fillCircle(balls[i].loc.x, balls[i].loc.y, balls[i].radius);
        }

        for (int i = 0; i < nballs - 1; i++)
        {
            Canvas_strokeLine(balls[i].loc.x, balls[i].loc.y, balls[i + 1].loc.x, balls[i + 1].loc.y);
        }



        Canvas_endDraw();

        sleepMillis(delay);
    }

    return 0;
}

float random_org(int x, int y)
{
    float k = (float)rand() / (float)(RAND_MAX / (y - x)) + x;
    return k;
}
